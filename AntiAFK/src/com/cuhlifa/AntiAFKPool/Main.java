package com.cuhlifa.AntiAFKPool;

import java.util.ArrayList;
import java.util.UUID;

import net.minecraft.server.v1_7_R3.EntityPlayer;
import net.minecraft.server.v1_7_R3.WorldServer;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_7_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;


public class Main extends JavaPlugin implements Listener {

	public static ArrayList<PlayerData> PlayerDataList = new ArrayList<PlayerData>();
	public static String KickMessage;
	public static Boolean KickPlayer;
	public static Boolean TeleportPlayer;
	public static Location TeleportLocation;
	public static int AFKTime;
	
	
	@Override
	public void onEnable(){

		getServer().getPluginManager().registerEvents(this, this);
		
		saveDefaultConfig();
		
		for(Player p : Bukkit.getOnlinePlayers()){
			
			createPlayerData(((CraftPlayer) p).getHandle().uniqueID);
			
		}
		
		LoadConfigOptions();
		
	}
	
	public void LoadConfigOptions(){
		
		
		KickMessage = getConfig().getString("KickMessage");
		KickPlayer = getConfig().getBoolean("KickPlayer");
		TeleportPlayer = getConfig().getBoolean("TeleportPlayer");
		TeleportLocation = new Location(Bukkit.getWorld(getConfig().getString("TeleportLocation.World")),  getConfig().getDouble("TeleportLocation.X"), getConfig().getDouble("TeleportLocation.Y"), getConfig().getDouble("TeleportLocation.Z"));
		TeleportPlayer = getConfig().getBoolean("TeleportPlayer");
		AFKTime = getConfig().getInt("AFKTime");
		
	}
	
	public static boolean hasPlayerData(UUID p){
		
		for(PlayerData pd : PlayerDataList){
			
			if(pd.p.equals(p)){return true;}
			
		}
		return false;
		
	}
	
	public static void createPlayerData(UUID p){
		
		if(!hasPlayerData(p)){
			
			PlayerDataList.add(new PlayerData(p));
			
		}
		
	}
	
	public static PlayerData getPlayerData(UUID p){
		
		for(PlayerData pd : PlayerDataList){
			
			if(pd.p.equals(p)){return pd;}
			
		}
		return null;
		
	}
	
	@EventHandler
	public void setPlayerConnection(PlayerJoinEvent e){
		
		
		((CraftPlayer) e.getPlayer()).getHandle().playerConnection = new PacketHandler(((CraftPlayer) e.getPlayer()).getHandle().server, ((CraftPlayer) e.getPlayer()).getHandle().playerConnection.networkManager, ((CraftPlayer) e.getPlayer()).getHandle());
		createPlayerData(((CraftPlayer) e.getPlayer()).getHandle().uniqueID);
		
		
		try {
	
			EntityPlayer player = ((CraftPlayer) e.getPlayer()).getHandle();
			java.lang.reflect.Field f = ((WorldServer) player.getWorld()).getClass().getField("tracker");
			f.setAccessible(true);
			f.set(((WorldServer) player.getWorld()), new Tracker(((WorldServer) player.getWorld())));
			
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}
	
}
