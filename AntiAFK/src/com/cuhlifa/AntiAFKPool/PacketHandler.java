package com.cuhlifa.AntiAFKPool;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.mysql.jdbc.Field;
import com.mysql.jdbc.jdbc2.optional.SuspendableXAConnection;

import net.minecraft.server.v1_7_R3.EntityPlayer;
import net.minecraft.server.v1_7_R3.EntityTracker;
import net.minecraft.server.v1_7_R3.EntityTrackerEntry;
import net.minecraft.server.v1_7_R3.MinecraftServer;
import net.minecraft.server.v1_7_R3.NetworkManager;
import net.minecraft.server.v1_7_R3.Packet;
import net.minecraft.server.v1_7_R3.PacketPlayInBlockDig;
import net.minecraft.server.v1_7_R3.PacketPlayInBlockPlace;
import net.minecraft.server.v1_7_R3.PacketPlayInEntityAction;
import net.minecraft.server.v1_7_R3.PacketPlayInFlying;
import net.minecraft.server.v1_7_R3.PacketPlayInLook;
import net.minecraft.server.v1_7_R3.PacketPlayInPosition;
import net.minecraft.server.v1_7_R3.PacketPlayInPositionLook;
import net.minecraft.server.v1_7_R3.PacketPlayOutBlockChange;
import net.minecraft.server.v1_7_R3.PacketPlayOutEntityHeadRotation;
import net.minecraft.server.v1_7_R3.PacketPlayOutEntityLook;
import net.minecraft.server.v1_7_R3.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_7_R3.PacketPlayOutPosition;
import net.minecraft.server.v1_7_R3.PacketPlayOutRelEntityMove;
import net.minecraft.server.v1_7_R3.PacketPlayOutRelEntityMoveLook;
import net.minecraft.server.v1_7_R3.PlayerConnection;
import net.minecraft.server.v1_7_R3.WorldServer;

public class PacketHandler extends PlayerConnection{
	
	public PacketHandler(MinecraftServer minecraftserver,
			NetworkManager networkmanager, EntityPlayer entityplayer) {
		super(minecraftserver, networkmanager, entityplayer);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void a(PacketPlayInFlying packet) {

		PlayerData pd = Main.getPlayerData(player.uniqueID);
		
		if(player.onGround && !player.isSleeping() && !player.isSprinting()){
			
			
			if(packet.g() == 0 && packet.h() == 0){
				
				pd.AfkState++;
				System.out.println(pd.AfkState);
								
				if(pd.AfkState > Main.AFKTime){
					
					Player bukkitplayer = Bukkit.getPlayer(player.uniqueID);
					
					if(!bukkitplayer.hasPermission("AFK.Bypass") && Main.KickPlayer){
						
						bukkitplayer.kickPlayer(ChatColor.translateAlternateColorCodes('&', Main.KickMessage));
						pd.AfkState = 0;
						
					}
					
					if(!bukkitplayer.hasPermission("AFK.Bypass") && !Main.KickPlayer && Main.TeleportPlayer){
						
						bukkitplayer.teleport(Main.TeleportLocation);
						pd.AfkState = 0;
						
					}
					
				}
				
			}else{
				
				pd.AfkState = 0;
				
			}
			
		}
		
		super.a(packet);
		
	}
	
	

}
